---
title: "Error Docker"
#title: "Error-Docker"
date: 2019-03-11T07:07:16+07:00
thumbnail: ""
sidebar: true
categories:
  - "Docker"
tags:
  - "docker"
  - "linux"
  - "container"
draft: false
---

## Error Starting Docker

baru belajar docker dan tentu saja selalu saja ada error yang bikin penasaran dan bikin susah merem, merem dikit langsung kepikiran keyword yang kira-kira cocok untuk selesaiin error docker ini dan kira-kira ini triknya.
mungkin masih banyak cara lain dan cara ini juga cuman ngikutin pesan yang direkomendasikan.
beberapa perintah yang harus dijalankan di terminal jika terjadi error seperti yang saya temui. 

* journalctl -xe
* systemctl status docker.service
* systemctl start docker.service
* journalctl -f -u docker
* dockerd

Done!
