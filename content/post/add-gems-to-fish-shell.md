---
title: "Add Gems to Fish Shell"
#title: "Add-Gems-to-Fish-Shell"
date: 2019-03-25T01:56:12+07:00
thumbnail: ""
sidebar: true
categories:
  - "Linux"
tags:
  - "Linux"
  - "Fish Shell"
  - "Ruby Gems"
  - "terminal"
draft: false
---

## tambah gems path ke config fish shell

ini nih error yang banyak terjadi saat install jekyll gems. goro-goro gak baca wiki dulu dan gak nyimak di forum.

WARNING: You don't have /home/sanoy/.gem/ruby/2.2.0/bin in your PATH, 
gem executables will not run. 
Successfully installed bundle-0.0.1 
Parsing documentation for bundle-0.0.1 Done installing

tapi yah karena banyak yang mengalami dan ini juga bukan hal yang baru, begitu juga di fish shell.
untuk mengatasinya cukup dengan menjalankan 1 perintah di terminal.

set -U fish_user_paths (ruby -e 'print Gem.user_dir')/bin $fish_user_paths

done!

langsung bisa jalanin jekyll. sebenarnya jekyll itu udah bisa di eksekusi tapi shell belum bisa membacanya jadi harus ditambahin manual ke shell yang di pakai. okelah segitu aja.
