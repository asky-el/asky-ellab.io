---
title: "Chsh PAM Authentication Failure"
#title: "Chsh-PAM-Authentication-Failure"
date: 2019-03-19T02:46:19+07:00
thumbnail: ""
sidebar: true
categories:
  - "Linux"
  - "Shell"
tags:
  - "fish shell"
  - "PAM authentication failure"
  - "Kali Linux"
draft: false
---

kasusnya adalah saya ingin menjadikan fish shell sebagai default shell, tapi saat menjalankan perintah chsh -s /usr/local/bin/fish, terjadi kesalahan. pesan PAM authentication failure muncul. bingung sih namanya juga error yang tidak disangka-sangka tapi tenang, ini sudah bukan masalah besar lagi.

Periksa directory dimana fish shell berada. pada kasus saya, fish shell berada di /usr/bin/fish. agar dapat terbaca, maka harus ditambahkan terlebih dahulu di /etc/shells. jika sudah, tinggal jalankan perintah chsh -s /usr/bin/fish
jika terjadi error seperti yang saya alami. chsh : PAM authentication failure
maka kita  perlu melihat isi dari /etc/passwd. error ini terjadi karena terjadi baris yang tidak sinkron dengan data, alias shell yang dijalankan tidak ada atau bisa juga karena kesalahan directory. contoh saya fish shell berada di directory /usr/bin/fish tapi di /etc/passwd, fish shell malah diarahkan ke /usr/local/bin/fish. setelah di periksa ternyata tidak file apapun disana. jadi yah untuk mengatasi masalah PAM auuthentication failure, kita hanya perlu memperbaiki kesalahan directory tersebut.
done masalah beres.
fish shell udah bisa dijadiin default.
