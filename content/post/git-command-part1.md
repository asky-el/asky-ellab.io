---
title: "Git Command Part1"
#title: "Git-Command-Part1"
date: 2019-03-25T02:03:21+07:00
thumbnail: ""
sidebar: true
categories:
  - "Git"
  - "Linux"
tags:
  - "Github git"
  - "Linux"
  - ""
draft: false
---

## Perintah Dasar git

* buat repository manual di github.com atau gitlab.com
* clone repository dengan <code>git clone https://github.com/uname/repo_name.git</code>
* masuk ke directory yang sudah di clone tadi dan tambahkan file/directory yang ingin kitamasukkan ke github. 
* lakukan add dengan cara <code>git add --all</code> . parameter --all ini bisa diganti hanya dengan tanda titik ( . )
* ketik <code>git status</code> untuk melihat apakah file/dir sudah siap di push.
* setelah itu ketik lagi <code>git commit -m "terserah isinya apa"</code> . commit ini untuk deskripsi kan perubahan atau apa yang sudah kita lakukan.
* dan terakhir push repository untuk memasukkan ke folder master yang berada di github. ketik<code> git push -u origin master</code>
* selesai.

## hapus files
perintah ini biasa dipakai saat ada kesalahan. misalnya saat file sudah di index tapi tidak bisa di push. saya juga bingung sih. tapi yang pastinya file itu ada di dalam cache jadi perlu di hapus.
* ketik <code>git rm --cached nama_file</code>
* ketik <code>git status</code> untuk melihat apakah sudah di hapus. 
* lakukan commit ulang dan push ulang repo tersebut <code> git commit -m "terserah isinya apa"</code>
* terakhir upload <code> git push -u origin master</code>

Done!
