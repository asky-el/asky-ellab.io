---
title: "Mind Mapping for Linux"
#title: "Mind-Mapping-for-Linux"
date: 2019-03-16T02:17:05+07:00
thumbnail: ""
sidebar: true
categories:
  - "Software Linux"
tags:
  - "Software"
  - "Linux"
  - "mind mapping"
draft: false
---
kenalan dulu. apa sih mind mapping itu?
mind mapping kadang sudah kita lakkan setiap saat, terutama dalam memikirkan suatu sebab akibat atau mengkategorikan sesuatu yang akan kita lakukan atau gunakan. contoh ketika kita akan membeli hp, tentu kita memerlukan kriteria agar tidak sampe salah beli, lalu kemudian kita akan mencocokkan dengan banyaknya jenis,merk dan model hp hingga kita memutuskan membeli salah satunya. tapi, apa sih sebenarnya mind mapping itu?
Mind mapping atau peta pikiran merupakan suatu cara yang digunakan untuk mempelajari suatu konsep berdasarkan bagaimana manusia menyimpan berbagai informasi kemudian menjabarkan informasi tersebut kedalam suatu metode. source : http://rajapresentasi.com/2014/12/apa-itu-mind-mapping-dan-bagaimana-cara-membuatnya/

setelah membaca contoh dan definisi, harusnya kita sudah dapat menyimpulkan betapa pentingnya mind mapping itu. mind mapping ini adalah suatu metode jadi dapat di aplikasikan kemana saja. disini kita tidak akan membahas bagaimana mengaplikasikan mind mapping ini tapi kita akan mengenal apa saja free software di linux untuk membuat mind mapping lebih menarik dalam bentuk visual. yuk lihat apa saja free software mind mapping itu karena software mind mapping ini sangat banyak. ada yang untuk windows, macOS dan berbasis web.
## freemind
## mindomo
## Freeplane
## mindmeister

dan masih banyak lagi. silahkan cari yang menyenangkan untuk dilihat. khusus macOS saya sangat suka melihat mindnode jadi mungkin software menjadi rekomendasi saya dan windows saya suka melihat ###nama_software

sekian reviewnya dan selamat memilih. good luck
