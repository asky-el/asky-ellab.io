---
title: "Perl Setting Locale Failed"
#title: "Perl-Setting-Locale-Failed"
date: 2019-03-25T23:10:15+07:00
sidebar: true
author: false
thumbnail: ""
categories:
  - "Pemrograman"
tags:
  - "Perl"
  - "Linux"
draft: false
---

# atasi error perl: warning: setting locale failed.

kadangkala pesan error semacam ini akan kita temukan. dalam kasusku, saya menggunakan kali linux d Docker. konfigurasi masih default. pesan ini muncul ketika saya menjalankan perintah apt.

> perl: warning: Setting locale failed.
> perl: warning: Please check that your locale settings:
>	LANGUAGE = (unset),
>	LC_ALL = (unset),
>	LC_CTYPE = "en_US.UTF-8",
>	LANG = (unset)


## Generating locales

Missing locales are generated with locale-gen:

> locale-gen en_US.UTF-8

Alternatively a locale file can be created manually with localedef:[1]

> localedef -i en_US -f UTF-8 en_US.UTF-8

## Setting Locale Settings

The locale settings can be set (to en_US.UTF-8 in the example) as follows:

> export LANGUAGE=en_US.UTF-8
> export LANG=en_US.UTF-8
> export LC_ALL=en_US.UTF-8
> locale-gen en_US.UTF-8
> dpkg-reconfigure locales



